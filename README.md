# SIDLOC Miscellaneous #

This repository contains miscellaneous source code that does not belong to any other repository.

## Harness
These diagrams are rendered using WireViz, providing clear visualizations of the harness layout and connectivity

[View latest generated harness](https://gitlab.com/librespacefoundation/sidloc/sidloc-miscellaneous/-/jobs/artifacts/master/browse/harness?job=wireviz)
[View latest generated connection diagram](https://gitlab.com/librespacefoundation/sidloc/sidloc-miscellaneous/-/jobs/artifacts/master/browse/harness?job=connviz)

## Power simulation

[Latest power simulation figures](https://gitlab.com/librespacefoundation/sidloc/sidloc-miscellaneous/-/jobs/artifacts/master/browse/simplots?job=power-sim)
